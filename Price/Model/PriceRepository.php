<?php
/**
 * @category: Magento
 * @package: CodeTest/Price
 * @author: Perficient <pratyush.mankar@perficient.com>
 */

namespace CodeTest\Price\Model;

use CodeTest\Price\Api\Data\PriceInterface;
use CodeTest\Price\Api\PriceRepositoryInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use CodeTest\Price\Logger\PriceLogger;
use CodeTest\Price\Helper\Data as PriceHelperData;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class PriceRepository implements PriceRepositoryInterface
{
    /**
     * @var PriceHelperData
     */
    private $priceHelperData;
    /**
     * @var PriceInterface
     */
    private $priceModel;
    /**
     * @var PriceLogger
     */
    private $pricelogger;
    /**
     * @var CurlFactory
     */
    private $curlFactory;
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @param PriceLogger $priceLogger
     * @param PriceHelperData $priceHelperData
     * @param PriceInterface $priceModel
     */
    public function __construct(
        PriceLogger $priceLogger,
        PriceHelperData $priceHelperData,
        PriceInterface $priceModel,
        CurlFactory $curlFactory,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->priceLogger = $priceLogger;
        $this->priceHelperData = $priceHelperData;
        $this->priceModel = $priceModel;
        $this->curlFactory = $curlFactory;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param int $productId
     * @return PriceInterface|null
     */
    public function get(int $productId): ?PriceInterface
    {
        try {
            if (empty($productId) || !is_int($productId)) {
                throw new \Exception(__('Product Id is not provided OR it is not in required type'));
            }

            $priceApiUrlWithProdId = $this->priceHelperData->getPriceApiEndpointUrl() . $productId;
            $response = $this->getPriceApiResponse($priceApiUrlWithProdId);

            if (is_array($response) && isset($response['data']['product_id'])
                && isset($response['data']['unit_price'])) {
                $this->priceModel->setProductId($response['data']['product_id']);
                $this->priceModel->setUnitPrice($response['data']['unit_price']);
                $price = $response['data']['unit_price'];
                $displayPrice = $this->priceCurrency->format($price);
                $this->priceModel->setDisplayPrice($displayPrice);
                return $this->priceModel;
            }
        } catch (\Exception $e) {
            $this->priceLogger->critical($e);
        }

        return null;
    }

    /**
     * Get price API response
     *
     * @param $priceApiUrlWithProdId string
     * @return array|null
     */
    private function getPriceApiResponse($priceApiUrlWithProdId): ?array
    {
        $decodedResponse = null;
        try {
            $curl = $this->curlFactory->create();
            $curl->addHeader("Content-Type", "application/json");
            $curl->addHeader("Accept", "application/json");

            $curl->get($priceApiUrlWithProdId);
            $response = $curl->getBody();

            if ($response == false) {
                throw new \Exception ( "Bad data." );
            }

            if (!empty($response)) {
                $this->priceLogger->info('Response'.$response);
                $decodedResponse = json_decode($response, true);
            }
        } catch (\Exception $e) {
            $this->priceLogger->critical($e);
        }

        return $decodedResponse;
    }
}
